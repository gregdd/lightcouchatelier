package service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.lightcouch.CouchDbClient;
import org.lightcouch.Response;

import beans.Description;
import beans.Film;

public class AtelierService {
    public static void createFilm(){
        //instancier un client
        CouchDbClient dbClient = new CouchDbClient();
        
        Description desc = new Description(Arrays.asList("pers1", "pers2", "pers3"), new Date());
        Film film0 = new Film("tintin", 86, desc);
        Film film1 = new Film("kirikou", 75, desc);
        Film film2 = new Film("avatar", 50, desc);

        //possibilite de recevoir la reponse du serveuren sauvegardant
        Response response = dbClient.save(film0);
        System.out.println(response.getError());
        dbClient.save(film1);
        dbClient.save(film2);
    }
    
    public static Film findFilmById(String id){
        CouchDbClient dbClient = new CouchDbClient();
        try {
            return dbClient.find(Film.class, id);  
        }catch (Exception e){
            System.out.println("id introuvable");
        }
        return null;
    }
    
    public static List<Film> findFilmByRatingView(int rating){
        CouchDbClient dbClient = new CouchDbClient();
        return dbClient.view("finder/movies_rating")
                .key(rating)
                .includeDocs(true)
                .query(Film.class);
    }
    
    public static List<Film> findFilmByMinRatingView(int minRating){
        CouchDbClient dbClient = new CouchDbClient();
        return dbClient.view("finder/movies_rating")
                .startKey(minRating)
                .includeDocs(true)
                .query(Film.class); 
    }
    
    public static void removeFilm(){
        CouchDbClient dbClient = new CouchDbClient();
        Film film = dbClient.find(Film.class, "1");
        dbClient.remove(film);
    }
}
