package beans;

public class Film {
    private String _id;
    private String _rev;
    private String name;
    private int rating;
    private Description desc;
    public static int idCounter = 0;

    public Film(String name, int rating, Description desc) {
        super();
        this.name = name;
        this.rating = rating;
        this.desc = desc;
        setId(Integer.toString(++idCounter));
    }

    public Film() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public Description getDesc() {
        return desc;
    }

    public void setDesc(Description desc) {
        this.desc = desc;
    }

    public String getId() {
        return _id;
    }

    public void setId(String id) {
        this._id = id;
    }

    public String get_rev() {
        return _rev;
    }

    public void set_rev(String _rev) {
        this._rev = _rev;
    }
}
