package beans;

import java.util.Date;
import java.util.List;

public class Description {

    private List<String> listProducers;
    private Date release;

    public Description(List<String> listProducers, Date release) {
        super();
        this.listProducers = listProducers;
        this.release = release;
    }

    public Description() {
        super();
    }

    public List<String> getListProducers() {
        return listProducers;
    }

    public void setListProducers(List<String> listProducers) {
        this.listProducers = listProducers;
    }

    public Date getRelease() {
        return release;
    }

    public void setRelease(Date release) {
        this.release = release;
    }
}
